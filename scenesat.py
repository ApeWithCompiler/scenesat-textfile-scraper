#!/usr/bin/python3

import sys
import time
import datetime
import requests

SCENESAT_URL="https://scenesat.com/playing"

DEFAULT_POLL=30

# Container for track information
# mostly simplifies time handling
class Track(object):
    def __init__(self, title, artist, duration, started):
        self._title = title
        self._artist = artist
        self._duration = datetime.timedelta(seconds=duration)
        self._started = started

    def title(self):
        return self._title

    def artist(self):
        return self._artist

    def duration(self):
        return self._duration

    def current(self):
        return datetime.datetime.now() - self._started

# Convert timedelta to 'mm:ss' string
def timedelta_str(td):
    sec = td.seconds % 60
    min = (td.seconds - sec) // 60
    return f'{min:02}:{sec:02}' 

# Calculate timestamp when started on scenesat
def calc_started(sc_now, sc_started):
    running_since = sc_now - sc_started
    return datetime.datetime.now() - datetime.timedelta(seconds=running_since)

# Parse the scenesat api response to json
def track_from_json(inp):
    title = inp["title"]
    artist = inp["artist"]
    duration = inp["duration"]

    sc_now = inp["now"]
    sc_started = inp["started"]

    return Track(title, artist, duration, calc_started(sc_now, sc_started))

# Scrape track from scenesat
def scrape_current_track(url):
    resp = requests.get(url)

    if resp.status_code != 200:
        sys.stderr.write("Failed scrape current track, status code: {}\n".format(resp.status_code))
        return None

    return resp.json()

# Format line for saving in the file
def fmt_line(track):
        return "[{}/{}] {} - {}".format(timedelta_str(track.current()), timedelta_str(track.duration()), track.artist(), track.title())

# Save in the file
# Current content needs to be overwrittten
def update_file(file, line):
    with open(file, "w") as f:
        f.write(line)

# Get timestamp when next update from scenesat is nedded
def next_poll_timestamp(seconds):
    if seconds is None:
        seconds = DEFAULT_POLL

    return datetime.datetime.now() + datetime.timedelta(seconds=seconds)

# Wrapper for scenesat request
def poll(url):
    track = scrape_current_track(url)
    if track is None:
        sys.stderr.write("No track, skipping update\n")
        return None

    nextpoll = track["nextpoll"]

    return next_poll_timestamp(nextpoll), track_from_json(track)

# Syntactic sugar for when need to update
def is_poll_needed(nextpoll):
    return (nextpoll is None or nextpoll < datetime.datetime.now())

def main():
    file = None
    if len(sys.argv) > 1:
        file = sys.argv[1]

    nextpoll = None
    track = None
    while True:
        if is_poll_needed(nextpoll): 
            nextpoll, track = poll(SCENESAT_URL)
            sys.stderr.write("Updated {} - {}\n".format(track.artist(), track.title()))
            sys.stderr.write("Polling again at {}\n".format(nextpoll))
        
        if track is not None:
            line = fmt_line(track)
            if file is not None:
                update_file(file, line)
            else:
                sys.stdout.write(line + "\n")
                sys.stdout.flush()

        time.sleep(1)

if __name__ == "__main__":
    main()
