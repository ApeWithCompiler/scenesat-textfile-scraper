# Scenesat Textfile Scraper

Scrapes the current track from scenesat in a textfile.
This can be used in obs as a text(file) source.

## Features

* Offline duration calculation
* Nice scraping - uses scenesats next poll intervall

## Dependencies

Only the `requests` library

## Usage

```bash
python3 ./scenesat.py /tmp/trackfile.txt
```

![terminal](screenshot_term.png)

## OBS

Create a new text input source. Select from textfile and select the corrosponding file.
The scraper will update the file with the current duration, artist and title. OBS should dynamicly update from the file.

![obs](screenshot_obs.png)
